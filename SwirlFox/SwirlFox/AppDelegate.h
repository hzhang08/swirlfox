//
//  AppDelegate.h
//  SwirlFox
//
//  Created by Hong Zhang on 3/3/14.
//  Copyright (c) 2014 Foxit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
