//
//  Shader.fsh
//  SwirlFox
//
//  Created by Hong Zhang on 3/3/14.
//  Copyright (c) 2014 Foxit. All rights reserved.
//

varying lowp vec4 colorVarying;

void main()
{
    gl_FragColor = colorVarying;
}
